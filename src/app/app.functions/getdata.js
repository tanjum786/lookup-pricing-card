const hubspot = require("@hubspot/api-client");

exports.main = async (context = {}, sendResponse) => {
  try {
    const { hs_object_id } = context.propertiesToSend;
    const associatedDeals = await getAssociatedDeals(hs_object_id);
    sendResponse(associatedDeals);
  } catch (error) {
    console.error(error);
    sendResponse({ error: error.message });
  }
};

async function getAssociatedDeals(hs_object_id) {
  const hubSpotClient = new hubspot.Client({
    accessToken: process.env.PRIVATE_APP_ACCESS_TOKEN,
  });

  const dealId = hs_object_id;
  const properties = undefined;
  const propertiesWithHistory = undefined;
  const associations = ["line_item", "companies"];
  const archived = false;
  const idProperty = undefined;

  try {
    const apiResponse = await hubSpotClient.crm.deals.basicApi.getById(
      dealId,
      properties,
      propertiesWithHistory,
      associations,
      archived,
      idProperty
    );

    // company data fetch api
    let CompanyId = apiResponse?.associations["companies"]?.results[0].id;
    const companyId = CompanyId;
    const comProperties = [
      "bits_boerange__c",
      "bits_company_size__c",
      "bits_segmentsector__c",
    ];
    const comAssociations = undefined;
    let CompanyBoe = null;
    let CompanySize = null;
    let CompanySector = null;

    const comApiResponse = await hubSpotClient.crm.companies.basicApi.getById(
      companyId,
      comProperties,
      propertiesWithHistory,
      comAssociations,
      archived,
      idProperty
    );
    CompanyBoe = comApiResponse?.properties.bits_boerange__c.toUpperCase();
    CompanySize = comApiResponse?.properties.bits_company_size__c;
    CompanySector = comApiResponse?.properties.bits_segmentsector__c;

    let companies = apiResponse?.associations["companies"]?.results;
    let lineItemsArray = apiResponse?.associations["line items"]?.results;
    let totalProductPrice = 0;
    let productSearchLength = null;
    let productPrice=0;
    let msg;
    if (lineItemsArray?.length > 0 || lineItemsArray != undefined) {
      for (const lineItem of lineItemsArray) {
        const lineitemProperties = ["amount", "product_code"];

        const Lineassociations = undefined;
        let lineItemProductCode = null;
        let lineItemId = lineItem.id;

        const lineItemApiResponse = await hubSpotClient.crm.lineItems.basicApi.getById(
          lineItemId,
          lineitemProperties,
          propertiesWithHistory,
          Lineassociations,
          archived,
          idProperty
        );

        lineItemProductCode = lineItemApiResponse.properties.product_code;
        productSector = null;
        productBoe = null;
        proCompanySize = null;

        let filterGroups;
        if (lineItemProductCode && CompanySector) {
          filterGroups = [
            {
              filters: [
                {
                  propertyName: "product_code",
                  operator: "EQ",
                  value: lineItemProductCode,
                },
                {
                  propertyName: "segment_sector",
                  operator: "EQ",
                  value: CompanySector,
                },
              ],
            },
          ];
        }

        const productCodeSearchQuery = {
          filterGroups: filterGroups,
          limit: 100,
          properties: [
            "boe_range",
            "name",
            "company_size",
            "product_code",
            "hs_sku",
            "segment_sector",
            "hs_price_cad",
            "price",
          ],
        };

        const productCodeSearchResponse = await hubSpotClient.crm.products.searchApi.doSearch(
          productCodeSearchQuery
        );

        let filteredProducts = [];
        productCodeSearchResponse.results.forEach((product) => {
          if (
            product.properties.boe_range &&
            CompanyBoe &&
            product.properties.boe_range.toUpperCase() != CompanyBoe
          )
            return;
          if (
            product.properties.company_size &&
            CompanySize &&
            product.properties.company_size != CompanySize
          )
            return;
          filteredProducts.push(product);
        });

        productId = filteredProducts[0]?.id;
        if (Number(lineItemApiResponse.properties.amount) === 0) {
          productPrice = filteredProducts[0]?.properties.hs_price_cad;
        } else {
          productPrice = Number(lineItemApiResponse.properties.amount);
          msg="Already";
        }
      
        let total = parseFloat(productPrice);
        totalProductPrice += total;

        productBoe = filteredProducts[0]?.properties.boe_range;
        productSector = filteredProducts[0]?.properties.segment_sector;
        proCompanySize = filteredProducts[0]?.properties.company_size;

        // update other product properties Api
        const UpDateProperties = {
          amount: productPrice,
          price: productPrice,
          boe_range: productBoe,
          segment_sector: productSector,
          company_size: proCompanySize,
        };
        const upLineItemId = lineItemId;
        if (Number(lineItemApiResponse.properties.amount) === 0) {
          const UpdateApiResponse = await hubSpotClient.crm.lineItems.basicApi.update(
            upLineItemId,
            { properties: UpDateProperties },
            idProperty
          );
        }
      }
      productPrice = total = 0;
    }
    
    // update deal Api amount
    let currentAmount = apiResponse.properties.amount;
    const updatedDeal = await hubSpotClient.crm.deals.basicApi.update(dealId, {
      properties: { amount: totalProductPrice },
    });
    return {
      updatedDeal,
      companies,
      lineItemsArray,
      productSearchLength,
      currentAmount,
      totalProductPrice,
      msg
    };
  } catch (e) {
    e.message === "HTTP request failed"
      ? console.error(JSON.stringify(e.response, null, 2))
      : console.error(e);
  }
}
