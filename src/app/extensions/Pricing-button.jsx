import React, { useState } from "react";
import {
  Alert,
  Button,
  Flex,
  LoadingSpinner,
  hubspot,
} from "@hubspot/ui-extensions";

hubspot.extend(({ runServerlessFunction, actions }) => (
  <Extension
    runServerless={runServerlessFunction}
    sendAlert={actions.addAlert}
  />
));

const Extension = ({ runServerless, sendAlert }) => {
  const [loading, setLoading] = useState(false);

  const handleButtonClick = () => {
    setLoading(true);

    runServerless({
      name: "getdata",
      propertiesToSend: ["hs_object_id"],
    })
      .then((result) => {
        if (result.response?.productSearchLength === 0) {
          sendAlert({
            message: "There Is No Matching Products",
            type: "danger",
          });
        } else if (
          result.response?.companies?.length <= 0 ||
          result.response?.companies === undefined ||
          result.response?.lineItemsArray?.length <= 0 ||
          result.response?.lineItemsArray === undefined ||
          result.response?.msg === ""
        ) {
          sendAlert({
            message:
              "No line item can be found. Please check the Primary Company as at least the Sector set. Then check the BOE  range and company size if applicable.",
            type: "danger",
          });
        } else if (result.response?.msg != "") {
          sendAlert({
            message:"Some of the line items have been updated successfully. Please refresh the page.",
            type: "success",
          });
        } else {
          if (
            result.response?.totalProductPrice ===
            Number(result.response?.currentAmount)
          ) {
            sendAlert({
              message: "Line Item is Already Updated",
              type: "success",
            });
          } else if (
            result.response?.companies?.length >= 1 ||
            result.response?.lineItemsArray?.length >= 1
          ) {
            sendAlert({
              message:
                "Line Item is updated Successfully, Please Refresh the page",
              type: "success",
            });
          }
        }
      })
      .catch((error) => {
        setLoading(false);
        console.error("Error occurred while fetching and updating:", error);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Flex direction="column" align="center" gap="large">
      <Button type="submit" variant="primary" onClick={handleButtonClick}>
        Look Up Pricing
      </Button>
      {loading && <LoadingSpinner label="Loading..." />}
    </Flex>
  );
};

export default Extension;
